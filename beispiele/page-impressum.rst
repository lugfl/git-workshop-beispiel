.. title: Impressum
.. slug: impressum
.. date: 2019-05-27 17:38:30 UTC+02:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text

Inhaltlich Verantwortlicher gemäß §10 Absatz 3 MDStV
====================================================

| Frank Agerholm
| Linux User Group Flensburg e.V.
| Roggenbogen 14
| 24941 Flensburg - Germany


EMail : frank(at)lugfl.de



Haftungshinweis
===============

Trotz sorgfältiger inhaltlicher Kontrolle übernehmen ich keine Haftung für die Inhalte externer Links. Für den Inhalt der verlinkten Seiten sind ausschliesslich deren Betreiber verantwortlich.

.. code-block:: python
   :number-lines:

   print("Our virtues and our failings are inseparable")
